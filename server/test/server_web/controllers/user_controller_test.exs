defmodule ServerWeb.UserControllerTest do
  use ServerWeb.ConnCase

  alias Server.Accounts
  alias Server.Accounts.User

  @create_attrs %{first_name: "paymahn", last_name: "moghadasian", email: "lol"}
  @update_attrs %{id: "some updated id"}
  @empty_attrs %{}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get conn, user_path(conn, :index)
      assert json_response(conn, 200) == []
    end
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post conn, user_path(conn, :create), @create_attrs
      assert %{"first_name" => "paymahn"} = json_response(conn, 200)

      conn = get conn, user_path(conn, :index)
      assert %{"first_name" => "paymahn"} = Enum.at(json_response(conn, 200), 0)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, user_path(conn, :create), @empty_attrs
      assert json_response(conn, 400) == "could not make user"
    end
  end


  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
