defmodule Server.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Server.Accounts.User

  # https://github.com/elixir-ecto/ecto/issues/840
  @derive {Poison.Encoder, except: [:__meta__]}

  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :public_key, :string
    field :oauth_id, :string

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :public_key, :email, :oauth_id])
    |> validate_required([:first_name, :last_name, :email])
  end

end
