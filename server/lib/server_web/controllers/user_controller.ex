defmodule ServerWeb.UserController do
  use ServerWeb, :controller

  alias Server.Accounts
  alias Server.Accounts.User

  action_fallback ServerWeb.FallbackController

  def index(conn, _params) do
    users = Accounts.list_users()
    json conn, Accounts.list_users()
 end

  def create(conn, params) do
    case Accounts.create_user(params) do
      {:ok, %User{} = user} ->
        json conn, user
      {:error, error} ->
        json put_status(conn, :bad_request), "could not make user"
    end
  end

  def show(conn, %{"id" => id} = user_params) do
    case Accounts.get_user(id) do
      nil ->
        {:ok, %User{} = user} = Accounts.create_user(user_params)
        render(conn, "show.json", user: user)
        IO.puts "creating user: " <> id
      user ->
        render(conn, "show.json", user: user)
    end
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end

  # plug for ensuring users are added to db
  def add_user_if_not_present(conn, opts) do
    claims = Map.get(conn.assigns, :joken_claims)
    %{"sub" => id} = claims
    case Accounts.get_user(id) do
      nil ->
        {:ok, %User{} = user} = Accounts.create_user(%{id: id})
        IO.puts "creating user: " <> id
      _ ->
        IO.puts "user already exists"
    end
    conn
  end
end
