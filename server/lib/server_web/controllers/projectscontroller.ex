# server_web/controllers/projectscontroller
defmodule ServerWeb.ProjectsController do
  use ServerWeb, :controller
  def index(conn, _params) do
    projects = [
      %{
        name: "Joe",
        files: [
          %{
            name: "index.html",
            content: "hello",
            filetype: "css"
          }]
      }
    ]
    json conn, projects
  end
end