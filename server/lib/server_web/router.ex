defmodule ServerWeb.Router do
  use ServerWeb, :router

  alias ServerWeb.JWTHelpers
#  alias ServerWeb.UserController

  @doc """
  Function that will serve as Plug for verifying metadata
  """
  # def add_user_if_not_present(conn, opts) do
  #   claims = Map.get(conn.assigns, :joken_claims)
  #   %{"sub": id} = claims
  #   IO.inspect claims
  #   conn
  # end

#  def add_user_if_not_present(conn, opts) do
#    UserController.add_user_if_not_present conn, opts
#  end

  pipeline :api do
    plug :accepts, ["json"]
    if System.get_env("MIX_ENV") == "prod" do
      plug Joken.Plug,
          verify: &ServerWeb.JWTHelpers.verify/0,
          on_error: &ServerWeb.JWTHelpers.error/2
      plug :add_user_if_not_present
    end
  end

  scope "/", ServerWeb do
    get "/index", ProjectsController, :index
  end

  scope "/api", ServerWeb do

    pipe_through :api

    get "/projects", ProjectsController, :index


    post "/users", UserController, :create
    get "/users", UserController, :index
  end
end
