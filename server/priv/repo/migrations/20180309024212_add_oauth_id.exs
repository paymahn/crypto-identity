defmodule Server.Repo.Migrations.AddOauthId do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :oauth_id, :string
    end

    create index(:users, [:oauth_id])
  end
end
