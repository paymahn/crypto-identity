#! /bin/bash

source app-env

cd client
yarn install
cd ..

(ls ./protos/*.proto | entr protoc --elm_out=./client/src ./protos/*.proto)&

(cd client && yarn start)&

wait
