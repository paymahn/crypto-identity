#!/bin/bash
source ./app-env
rm -r bin

mkdir bin
# compile client
cd client
elm-make src/Main.elm --output=../bin/index.html
cd ..

echo "done!"

aws s3 sync bin s3://$S3_BUCKET --acl public-read

