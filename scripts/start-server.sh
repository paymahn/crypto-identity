#! /bin/zsh
source app-env

echo $AUTH0_PUBLIC_KEY > server/public_key.pem

cd server
mix deps.get
(mix phx.server)&

wait
