module Search.View exposing (view)

import Search.Models exposing (SearchModel)
import Element exposing (..)
import Msgs exposing (Msg(..))
import Element.Attributes exposing (..)
import Element.Input as Input
import Element.Events exposing (..)
import Models exposing (Model, User)
import Person.Models exposing (Person)
import Stylesheet exposing (Styles(..))
import ViewHelpers exposing (..)


view : Model -> SearchModel -> Element Styles variation Msg
view model searchModel =
    searchPage model


searchPage : Model -> Element Styles variation Msg
searchPage model =
    pageTemplate <|
        column NoStyle
            []
            [ searchBar ""
            , resultList []
            ]


resultList : List Person -> Element Styles variation Msg
resultList results =
    let
        entry result =
            row NoStyle
                []
                [ text result.name
                , text result.publicKey
                ]
    in
        column NoStyle
            []
            (List.map entry results)


header : Element Styles variation Msg
header =
    el Header [ padding 20 ] (text "cryptosearch")


searchBar : String -> Element Styles variation Msg
searchBar searchQuery =
    let
        searchText =
            { onChange = SearchQueryUpdate
            , value = searchQuery
            , label =
                (Input.placeholder
                    { text = "Search here"
                    , label = Input.labelRight searchButton
                    }
                )
            , options = []
            }
    in
        el NoStyle
            [ center ]
            (Input.search SearchRow [ width (content), center ] searchText)


searchButton : Element Styles variation Msg
searchButton =
    button Button [ center, padding 10 ] (text "search")
