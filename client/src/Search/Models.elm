module Search.Models exposing (..)

import Person.Models exposing (Person)


type alias SearchModel =
    { searchQuery : String
    , results : List Person
    }


initialSearchModel : SearchModel
initialSearchModel =
    { searchQuery = ""
    , results =
        [ Person "Aaron Peddle" "google.com" "123456"
        , Person "Paymahn Moghadasian" "google.com" "123456"
        , Person "Someone Else" "google.com" "123456"
        ]
    }
