module Main exposing (..)

import Html exposing (program)
import Msgs exposing (Msg(..))
import Models exposing (Model, initialModel)
import Update exposing (update)
import View exposing (view)
import Auth0
import Authentication
import Ports exposing (..)


init : Maybe Auth0.LoggedInUser -> ( Model, Cmd Msg )
init initialUser =
    let
        model =
            initialModel initialUser

        initCommand =
            Cmd.none
    in
        ( model, initCommand )


subscriptions : a -> Sub Msg
subscriptions model =
    auth0authResult (Authentication.handleAuthResult >> AuthenticationMsg)



-- MAIN


main : Program (Maybe Auth0.LoggedInUser) Model Msg
main =
    Html.programWithFlags
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }
