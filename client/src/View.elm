module View exposing (..)

import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (..)
import Maybe
import Msgs exposing (Msg(..))
import Models exposing (..)
import Search.View as SearchView
import Register.View as RegisterView
import Authentication
import Element exposing (Element)
import Stylesheet exposing (stylesheet)


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div [ class "jumbotron text-center" ]
            [ div []
                (case Authentication.tryGetUserProfile model.authModel of
                    Nothing ->
                        [ p [] [ text "Please log in" ] ]

                    Just user ->
                        [ page model ]
                )
            , p []
                [ button
                    [ class "btn btn-primary"
                    , onClick
                        (AuthenticationMsg
                            (if Authentication.isLoggedIn model.authModel then
                                Authentication.LogOut
                             else
                                Authentication.ShowLogIn
                            )
                        )
                    ]
                    [ text
                        (if Authentication.isLoggedIn model.authModel then
                            "Log Out"
                         else
                            "Log In"
                        )
                    ]
                ]
            ]
        ]


page : Model -> Html Msg
page model =
    Element.layout stylesheet <|
        case model.page of
            SearchPage searchModel ->
                SearchView.view model searchModel

            RegisterPage registerModel ->
                RegisterView.view model registerModel
