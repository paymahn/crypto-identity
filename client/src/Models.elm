module Models exposing (..)

import Authentication
import Auth0
import Ports exposing (..)
import Search.Models exposing (SearchModel)
import Register.Models exposing (RegisterModel, initialRegisterModel)
import Person.Models exposing (Person)


type alias Model =
    { authModel : Authentication.Model
    , page : Page
    , user : Maybe User
    }


type Page
    = SearchPage SearchModel
    | RegisterPage RegisterModel


type alias User =
    Person


initialModel : Maybe Auth0.LoggedInUser -> Model
initialModel initialUser =
    { page = RegisterPage initialRegisterModel
    , authModel = Authentication.init auth0authorize auth0logout initialUser
    , user = Nothing
    }
