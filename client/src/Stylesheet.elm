module Stylesheet exposing (..)

import Style.Shadow as Shadow
import Style.Border as Border
import Style.Color as Color
import Style.Font as Font
import Style exposing (..)
import Color exposing (..)


type Styles
    = NoStyle
    | Button
    | Parent
    | SearchRow
    | Header
    | Signup


searchRowFontSize =
    30


stylesheet =
    Style.styleSheet
        [ Style.style NoStyle []
        , Style.style Button
            [ Color.text lightGrey
            , Color.background darkGray
            , Font.size searchRowFontSize -- all units given as px
            , Border.rounded 2
            , hover
                [ Shadow.simple
                , Color.background black
                ]
            , focus
                [ Color.background lightGreen
                , Shadow.simple
                ]
            ]
        , Style.style SearchRow
            [ Font.size searchRowFontSize ]
        , Style.style Header
            [ Font.size 60
            , Font.center
            , Color.text charcoal
            ]
        ]
