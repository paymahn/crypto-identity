module Register.Models exposing (..)


type alias RegisterModel =
    -- data type for maintaining state of the Register view
    { publicKeyField : String
    }


initialRegisterModel : RegisterModel
initialRegisterModel =
    RegisterModel ""
