module Register.View exposing (view)

import Element exposing (..)
import Msgs exposing (Msg(..))
import Element.Attributes exposing (..)
import Element.Input as Input
import Element.Events exposing (..)
import Models exposing (Model, User)
import Register.Models exposing (..)
import Stylesheet exposing (Styles(..))
import ViewHelpers exposing (..)


view : Model -> RegisterModel -> Element Styles variation Msg
view model registerModel =
    -- An el is the most basic element, like a <div>
    pageTemplate <|
        column Signup
            []
            [ text "Register you public key"
            , Input.text NoStyle
                []
                { onChange = RegisterFieldUpdate
                , value = ""
                , label = Input.placeholder { text = "your public key", label = Input.hiddenLabel "your public key" }
                , options = []
                }
            , button Button [ onClick RegisterUser, center, padding 10 ] (text "register")
            ]
