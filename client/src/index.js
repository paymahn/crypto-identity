'use strict';

require('ace-css/css/ace.css');
require('font-awesome/css/font-awesome.css');

// Require index.html so it gets copied to dist
require('./index.html');

var Elm = require('./Main.elm');
//
//javascript often abuses undefineds to mean null. This confuses Elm ports and will throw a run time error.
//We manually set undefineds for values we expect to be either null or present here
function ensureProfileFields(profile){
  var defaultValues = {"email": null, "email_verified": false};

  Object.keys(defaultValues).forEach(function(field){
    if(!profile[field]) profile[field] = defaultValues[field];
  });

  return profile;
}

var webAuth = new auth0.WebAuth({
  domain:  process.env.AUTH0_DOMAIN,// e.g., you.auth0.com
  clientID: process.env.AUTH0_CLIENT_ID,
  scope: 'email',
  responseType: 'id_token',
  redirectUri: 'http://localhost:3000'
});
var storedProfile = localStorage.getItem('profile');
var storedToken = localStorage.getItem('token');
var authData = storedProfile && storedToken ? { 
    profile: ensureProfileFields(JSON.parse(storedProfile)),
    token: storedToken } : null;
var elmApp = Elm.Main.fullscreen(authData);
// Auth0 authorize subscription
elmApp.ports.auth0authorize.subscribe(function(opts) {
  webAuth.authorize();
});
// Log out of Auth0 subscription
elmApp.ports.auth0logout.subscribe(function(opts) {
  localStorage.removeItem('profile');
  localStorage.removeItem('token');
});
// Watching for hash after redirect

webAuth.parseHash({ hash: window.location.hash }, function(err, authResult) {
  console.log('test');
  if (err) {
    console.error("failed to parse hash");
    return console.error(err);
  }
  if (authResult) {
    console.log(authResult);

    var token = authResult.idToken;
    var profile = ensureProfileFields({id: authResult.idTokenPayload.sub});
    var result = {err: null, ok: null};
    result.ok = { profile: profile, token: token };

    localStorage.setItem('profile', JSON.stringify(profile));
    localStorage.setItem('token', token);

    elmApp.ports.auth0authResult.send(result);
    // webAuth.client.userInfo(authResult.idToken, function(err, profile) {
    //   var result = { err: null, ok: null };
    //   var token = authResult.idToken;
    //   if (err) {
    //     console.error("user info error");
    //     console.error(err);
    //     result.err = err.details;
    //     // Ensure that optional fields are on the object
    //     result.err.name = result.err.name ? result.err.name : null;
    //     result.err.code = result.err.code ? result.err.code : null;
    //     result.err.statusCode = result.err.statusCode ? result.err.statusCode : null;
    //   }
    //   if (authResult) {
    //     profile = ensureProfileFields(profile);
    //     result.ok = { profile: profile, token: token };

    //     localStorage.setItem('profile', JSON.stringify(profile));
    //     localStorage.setItem('token', token);
    //   }
    //   elmApp.ports.auth0authResult.send(result);
    // });
    window.location.hash = '';
  }
});

