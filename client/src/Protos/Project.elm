module Protos.Project exposing (..)


import Json.Decode as JD
import Json.Encode as JE


(<$>) : (a -> b) -> JD.Decoder a -> JD.Decoder b
(<$>) =
    JD.map


(<*>) : JD.Decoder (a -> b) -> JD.Decoder a -> JD.Decoder b
(<*>) f v =
    f |> JD.andThen (\x -> x <$> v)


optionalDecoder : JD.Decoder a -> JD.Decoder (Maybe a)
optionalDecoder decoder =
    JD.oneOf
        [ JD.map Just decoder
        , JD.succeed Nothing
        ]


requiredFieldDecoder : String -> a -> JD.Decoder a -> JD.Decoder a
requiredFieldDecoder name default decoder =
    withDefault default (JD.field name decoder)


optionalFieldDecoder : String -> JD.Decoder a -> JD.Decoder (Maybe a)
optionalFieldDecoder name decoder =
    optionalDecoder (JD.field name decoder)


repeatedFieldDecoder : String -> JD.Decoder a -> JD.Decoder (List a)
repeatedFieldDecoder name decoder =
    withDefault [] (JD.field name (JD.list decoder))


withDefault : a -> JD.Decoder a -> JD.Decoder a
withDefault default decoder =
    JD.oneOf
        [ decoder
        , JD.succeed default
        ]


optionalEncoder : String -> (a -> JE.Value) -> Maybe a -> Maybe (String, JE.Value)
optionalEncoder name encoder v =
    case v of
        Just x ->
            Just ( name, encoder x )

        Nothing ->
            Nothing


requiredFieldEncoder : String -> (a -> JE.Value) -> a -> a -> Maybe ( String, JE.Value )
requiredFieldEncoder name encoder default v =
    if v == default then
        Nothing
    else
        Just ( name, encoder v )


repeatedFieldEncoder : String -> (a -> JE.Value) -> List a -> Maybe (String, JE.Value)
repeatedFieldEncoder name encoder v =
    case v of
        [] ->
            Nothing
        _ ->
            Just (name, JE.list <| List.map encoder v)


type alias File =
    { name : String -- 1
    , contents : String -- 2
    , filetype : String -- 3
    }


fileDecoder : JD.Decoder File
fileDecoder =
    JD.lazy <| \_ -> File
        <$> (requiredFieldDecoder "name" "" JD.string)
        <*> (requiredFieldDecoder "contents" "" JD.string)
        <*> (requiredFieldDecoder "filetype" "" JD.string)


fileEncoder : File -> JE.Value
fileEncoder v =
    JE.object <| List.filterMap identity <|
        [ (requiredFieldEncoder "name" JE.string "" v.name)
        , (requiredFieldEncoder "contents" JE.string "" v.contents)
        , (requiredFieldEncoder "filetype" JE.string "" v.filetype)
        ]


type alias Project =
    { name : String -- 1
    , files : List Proto.Test.File -- 2
    }


projectDecoder : JD.Decoder Project
projectDecoder =
    JD.lazy <| \_ -> Project
        <$> (requiredFieldDecoder "name" "" JD.string)
        <*> (repeatedFieldDecoder "files" proto_Test_FileDecoder)


projectEncoder : Project -> JE.Value
projectEncoder v =
    JE.object <| List.filterMap identity <|
        [ (requiredFieldEncoder "name" JE.string "" v.name)
        , (repeatedFieldEncoder "files" proto_Test_FileEncoder v.files)
        ]
