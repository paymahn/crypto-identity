module Msgs exposing (..)

import Authentication


type Msg
    = AuthenticationMsg Authentication.Msg
    | SearchQueryUpdate String
    | RegisterFieldUpdate String
    | RegisterUser
