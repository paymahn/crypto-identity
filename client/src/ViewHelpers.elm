module ViewHelpers exposing (..)

import Search.Models exposing (SearchModel)
import Html exposing (Html)
import Element exposing (..)
import Msgs exposing (Msg(..))
import Style exposing (..)
import Element.Attributes exposing (..)
import Element.Input as Input
import Element.Events exposing (..)
import Stylesheet exposing (Styles(..))


header =
    el Header [ padding 20 ] (text "cryptosearch")


footer =
    text ""


pageTemplate : Element Styles variation Msg -> Element Styles variation Msg
pageTemplate content =
    column NoStyle
        []
        [ header
        , content
        , footer
        ]
