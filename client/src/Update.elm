module Update exposing (..)

import Msgs exposing (Msg(..))
import Models exposing (..)
import Search.Models exposing (..)
import Authentication
import Commands


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AuthenticationMsg authMsg ->
            let
                ( authModel, cmd ) =
                    Authentication.update authMsg model.authModel

                newModel =
                    { model | authModel = authModel }
            in
                ( newModel
                , Cmd.batch
                    [ Cmd.map AuthenticationMsg cmd
                    ]
                )

        SearchQueryUpdate msg ->
            ( model, Cmd.none )

        RegisterFieldUpdate msg ->
            ( model, Cmd.none )

        RegisterUser ->
            ( { model | page = SearchPage initialSearchModel }, Cmd.none )
