module Person.Models exposing (..)


type alias Person =
    { name : String
    , url : String
    , publicKey : String
    }
