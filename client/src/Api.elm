module Api exposing (..)

import Json.Decode exposing (Decoder)
import Auth0 exposing (Token)
import Http

domain =
  "http://localhost:3001/api/"

generateAuthenticatedGetRequest : Token -> String -> Decoder a -> Http.Request a
generateAuthenticatedGetRequest token resoucePath decoder=
    { method = "GET"
    , headers = [ Http.header "Authorization" ("Bearer " ++ token) ]
    , url = domain ++ resoucePath
    , body = Http.emptyBody
    , expect = Http.expectJson decoder
    , timeout = Nothing
    , withCredentials = False
    }
        |> Http.request
