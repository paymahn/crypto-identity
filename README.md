# Web Template

## Setup Auth0
adjust settings in app-env to match your auth0 settings

## Dev setup
install phoenix

mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez

install entr (brew install entr, apt-get install entr #probably)


install protobufs and elm protobuf support 

(instructions here https://github.com/tiziano88/elm-protobuf)

## Setting up the Server
cd server

mix deps.get

cd assets && npm install && node node_modules/brunch/bin/brunch build # probably not needed

mix ecto.create

## Running The Client
./scripts/run-client.sh

## Running the Server
./scripts/run-server.sh

## Testing server
cd server

mix test

## Deployment
deploy with:

./scripts/deploy.sh
